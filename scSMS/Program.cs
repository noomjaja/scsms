﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;
using Microsoft.Win32;
using Oracle.DataAccess.Client;


namespace scSMS
{
    class Program
    {
        static void Main(string[] args)
        {
            if(regisTryGetApp("Mode") == "Dev") {   
                ofMainMenu();
            } else {
                ofWaitingMsg();///Waiting Message Until Esc
                Console.Clear();
                ofMainMenu();
            }
        }

        static void ofMainMenu()
        {

            MainMenu:
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("*******************************Main Menu******************************");
            Console.WriteLine("1. Test Connect DataBase");
            Console.WriteLine("2. Check Credit SMS");
            Console.WriteLine("3. Waiting Message");
            Console.WriteLine("4. Setting");
            Console.WriteLine("5. Exit\n");
            Console.Write("Please Select Menu: ");
            try {
                byte menu = byte.Parse(Console.ReadLine());
                if(menu == 1) {
                    try {
                        Console.Clear();
                        ofTestConnectDb();
                        Console.Write("GO to Main Menu (y),(n) : ");
                        string vals = Console.ReadLine();
                        if(vals == "y") {
                            Console.Clear();
                            goto MainMenu;
                        }
                    } catch(Exception err) {
                        Console.WriteLine("error--->"+err.Message);
                        Console.Write("GO to Main Menu (y),(n) : ");
                        string vals = Console.ReadLine();
                        if(vals == "y") {
                            Console.Clear();
                            goto MainMenu;
                        }
                    }
                } else if(menu == 2) {
                    ofCheckCredit();
                } else if(menu == 3) {
                    ofWaitingMsg();
                    Console.Clear();
                    goto MainMenu;
                } else if(menu == 4) {
                    ofSettingCredit();
                } else if(menu == 5) {
                    
                } else {
                    Console.Clear();
                    Console.WriteLine("Please Select 1 -3 Menu");
                    Thread.Sleep(2000);
                    Console.Clear();
                    goto MainMenu;
                }
            } catch(Exception e) {
                Console.Clear();
                Console.WriteLine("Error-->" + e.Message);
                Console.Clear();
                goto MainMenu;
            }
        }

        static void ofWaitingMsg()
        {
            Console.Clear();
            ConsoleKeyInfo cki;
            int count = 0;
            int conMsg = Convert.ToInt16(regisTryGetApp("ContinueMsg"));
            string smsDocNo = string.Empty;
            Console.ForegroundColor = ConsoleColor.DarkRed;
            do {

                while(Console.KeyAvailable == false) {
                    Console.WriteLine("Waiting Message For Send SMS.....");
                    /*Get Message*/
                    try {
                        smsDocNo = pkFunction("pka_sms.fp_get_docno_waiting");
                    } catch {
                        smsDocNo = string.Empty;
                    }
                    /*Send Message*/
                    if(smsDocNo != string.Empty) {
                        try {
                            ofSendMessage(smsDocNo);
                            Console.WriteLine("Send SMS.....Success");
                            
                            if(regisTryGetApp("Mode") == "Dev") {
                                count++;
                                if(count >= conMsg) {
                                    goto endLoop;/*ถ้าเป็น DevMode จะต่อเนื่องได้ตามที่กำหนด*/
                                }
                            }
                        } catch(Exception e) {
                            Console.WriteLine("c87:error -->" + e.Message);
                        }
                    }
                    Thread.Sleep(1500);
                }

                cki = Console.ReadKey(true);
            } while(cki.Key != ConsoleKey.Escape);

            endLoop:
            Console.Clear();
            ofMainMenu();
        }

        static string pkFunction(string fnName)
        {

            string oradb = "Data Source=(DESCRIPTION ="
                    + "(ADDRESS = (PROTOCOL = TCP)(HOST = " + regisTryGetDB("hostname") + ")(PORT = 1521))"
                    + "(CONNECT_DATA =" + "(SERVER = DEDICATED)" + "(SERVICE_NAME = " + regisTryGetDB("servername") + ")));"
                    + "User Id= dbo;Password=xsqlx;";

            OracleConnection conn = new OracleConnection(oradb);
            conn.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn;
            cmd.CommandText = "select " + fnName + " from dual";
            cmd.CommandType = CommandType.Text;
            OracleDataReader dr = cmd.ExecuteReader();
            dr.Read();
            string vals = dr.GetString(0);
            conn.Dispose();

            return vals;
        }

        static void pkProcedure(string pkName)
        {
            string oradb = "Data Source=(DESCRIPTION ="
                    + "(ADDRESS = (PROTOCOL = TCP)(HOST = " + regisTryGetDB("hostname") + ")(PORT = 1521))"
                    + "(CONNECT_DATA =" + "(SERVER = DEDICATED)" + "(SERVICE_NAME = " + regisTryGetDB("servername") + ")));"
                    + "User Id= dbo;Password=xsqlx;";

            using(OracleConnection conn = new OracleConnection(oradb)) {
                using(OracleCommand cmd = new OracleCommand("begin " + pkName + "; end;", conn)) {

                    conn.Open();
                    OracleTransaction trans = conn.BeginTransaction();
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                    trans.Commit();
                }
            }

        }

        static void ofTestConnectDb()
        {
            string sid = regisTryGetDB("servername"), host = regisTryGetDB("hostname");
            string oradb = "Data Source=(DESCRIPTION ="
                    + "(ADDRESS = (PROTOCOL = TCP)(HOST =  "+ host + ")(PORT = 1521))"
                    + "(CONNECT_DATA =" + "(SERVER = DEDICATED)" + "(SERVICE_NAME =  "+ sid + ")));"
                    + "User Id= dbo;Password=xsqlx;";

            Console.WriteLine("Start Connect Database SID=" + sid + " Host = " + host);
            OracleConnection conn = new OracleConnection(oradb);
            conn.Open();
            Console.WriteLine("Connect Database Success!!");
            conn.Close();
            conn.Dispose();

        }

        static void ofCheckCredit()
        {
            Console.Clear();
            string userN = regisTryGetApp("sms_user");
            string pass = regisTryGetApp("sms_pass");
            string API_URL = "http://member.smsmkt.com/SMSLink/GetCredit/index.php";
            String Parameters = String.Format("?User={0}&Password={1}", userN, pass);

            WebClient webc = new WebClient();
            String Result = webc.DownloadString(API_URL + Parameters);
            Console.WriteLine("SMS Credit  : " + Result.Substring(Result.LastIndexOf("=") + 1));
            Console.WriteLine("Back to MainMenu in 2 sec.");
            Thread.Sleep(2000);
            Console.Clear();
            ofMainMenu();
        }

        static string regisTryGetDB(string key)
        {
          
            return regisTryGet(key,"database","native");

        }

        static string regisTryGetApp(string key)
        {

            return regisTryGet(key, "Program", "sms");

        }

        static string regisTryGet(string key,string SubKey,string ParentKey)
        {

            RegistryKey reg = Registry.CurrentUser.OpenSubKey("Software").OpenSubKey("SOAT").OpenSubKey("COOP").OpenSubKey(SubKey).OpenSubKey(ParentKey);
            string currentKey;
            

            try {
                currentKey = reg.GetValue(key).ToString();
            } catch {
                return String.Empty;
            }

            return currentKey;

        }

        static void ofSendMessage(string docno)
        {

            try {
                String Username = regisTryGetApp("sms_user");
                String Password = regisTryGetApp("sms_pass");
                String SenderName = regisTryGetApp("sms_sender");
                string ls_rc = string.Empty;
                object PhoneList = null, Message = null;
                ls_rc = pkFunction("pka_sms.fp_get_data_send('" + docno + "')");
                Dictionary<string, object> vals = toDictionary(toList(ls_rc));
                vals.TryGetValue("telephone_no", out PhoneList);
                vals.TryGetValue("message", out Message);

                Byte[] ByteMsg = System.Text.Encoding.GetEncoding("TIS-620").GetBytes((string)Message);
                Message = HttpUtility.UrlEncode(ByteMsg);

                String ParamFormat = "?User={0}&Password={1}&Msnlist={2}&Msg={3}&Sender={4}";
                String Parameters = String.Format(ParamFormat, Username, Password, PhoneList, Message, SenderName);
                String API_URL = "http://member.smsmkt.com/SMSLink/SendMsg/index.php";

                WebClient webc = new WebClient();
                String Result = webc.DownloadString(API_URL + Parameters);

                pkProcedure("pka_sms.sp_post_sms_send('" + docno + "','" + Result.Substring(Result.IndexOf("=") + 1, 1) + "','"
                    + Result.Substring(Result.LastIndexOf("=") + 1) + "')");


            } catch(Exception e) {
                Console.WriteLine("Error-->" + e.Message);
                return;
            }
        }

        static List<string> toList(string s, char separator = '&')
        {
            return s.TrimEnd().Split(separator).ToList();
        }

        static Dictionary<string, object> toDictionary(List<string> s, char separatorKey = '=')
        {
            Dictionary<string, object> v = new Dictionary<string, object>();
            foreach(var pair in s.ToDictionary(x => x.Substring(0, x.IndexOf('=')), x => x.Substring(x.IndexOf('=') + 1))) {
                v.Add(pair.Key, pair.Value);
            }
            return v;
        }

        static void ofSettingCredit() {
            RegistryKey reg = Registry.CurrentUser.OpenSubKey("Software").OpenSubKey("SOAT").OpenSubKey("COOP").OpenSubKey("Program").OpenSubKey("sms",true);
            
            Console.Clear();
            Console.WriteLine("UserName : " + regisTryGetApp("sms_user"));
            Console.WriteLine("Pass : " + regisTryGetApp("sms_pass"));
            Console.WriteLine("Sender : " + regisTryGetApp("sms_sender")+"\n");
            
            Console.Write("Goto Setting (y),(n) : ");
            string vals = Console.ReadLine();
            if(vals == "y") {
                Console.Clear();
                Console.Write("user name : ");
                vals = Console.ReadLine();
                if(!string.IsNullOrWhiteSpace(vals)) {
                    reg.SetValue("sms_user", vals);
                }
                Console.Write("PassWord : ");
                vals = Console.ReadLine();
                if(!string.IsNullOrWhiteSpace(vals)) {
                    reg.SetValue("sms_pass", vals);
                }
                Console.Write("Sender Name : ");
                vals = Console.ReadLine();
                if(!string.IsNullOrWhiteSpace(vals)) {
                    reg.SetValue("sms_sender", vals);
                }
                Console.Write("Setting Success!!  gotoMainMenu in 2 sec");
                Thread.Sleep(2000);
                Console.Clear();
                ofMainMenu();

            } else {
                Console.Clear();
                ofMainMenu();
            }
        }
    }
}
